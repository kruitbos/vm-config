#!/bin/sh

# Run as root prompt
if [ "$(id -u)" -ne 0 ]
then
  echo "Please run this script as root"
  exit 1
fi

# Update prompt
echo "Make sure your system is updated. Do you want to update this system right now?"
while true; do
    read -rp "(Y/n) " yn
    case $yn in
        [Yy]* ) apt update && apt upgrade -y && reboot -h now; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done

# Set timezone
echo "Setting Timezone to Europe/Amsterdam"
timedatectl set-timezone Europe/Amsterdam

# QEMU Guest Agent
echo "Is this a VM? Answering yes will install the QEMU guest agent."
while true; do
    read -rp "(Y/n) " yn
    case $yn in
        [Yy]* ) apt install qemu-guest-agent -y; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done

# Automatic upgrades
echo "Installing unattended-upgrades for automatic updates."
apt install unattended-upgrades -y

echo "Configuring unattended-upgrades"
AUTO_PATH=/etc/apt/apt.conf.d/20auto-upgrades
AUTO_PATH_BAK=/etc/apt/apt.conf.d/20auto-upgrades.bak

UNATTENDED_PATH=/etc/apt/apt.conf.d/50unattended-upgrades
UNATTENDED_PATH_BAK=/etc/apt/apt.conf.d/50unattended-upgrades.bak

mv $AUTO_PATH $AUTO_PATH_BAK && mv $UNATTENDED_PATH $UNATTENDED_PATH_BAK
cp ./var/20auto-upgrades $AUTO_PATH && cp ./var/50unattended-upgrades $UNATTENDED_PATH
chmod 611 $AUTO_PATH $UNATTENDED_PATH

systemctl start unattended-upgrades
systemctl enable unattended-upgrades

echo "Checking status unattended-upgrades"
systemctl status unattended-upgrades | head -8
sleep 5

echo "Setting up ssh"
mkdir /home/$SUDO_USER/.ssh
cat ./var/id_rsa.pub >> /home/$SUDO_USER/.ssh/authorized_keys
chmod 700 /home/$SUDO_USER/.ssh
chown $SUDO_USER /home/$SUDO_USER/.ssh

echo "Installing fail2ban"
apt install fail2ban -y
systemctl enable fail2ban
systemctl start fail2ban
systemctl status fail2ban | head -8
sleep 5

# Backup the current SSH configuration
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak

# Disable password authentication
sed -i 's/^#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/^PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/^#PasswordAuthentication no/PasswordAuthentication no/' /etc/ssh/sshd_config

# Ensure other relevant settings are set correctly
sed -i 's/^#ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/' /etc/ssh/sshd_config
sed -i 's/^ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/' /etc/ssh/sshd_config
sed -i 's/^#ChallengeResponseAuthentication no/ChallengeResponseAuthentication no/' /etc/ssh/sshd_config

# Restart SSH service to apply changes
systemctl restart sshd

echo "Password authentication over SSH has been disabled."

echo "installing UFW"
apt install ufw -y
ufw allow 22/tcp
ufw enable
echo "UFW enabled and added port 22/tcp"

echo "Script ran succesfully!"
echo "Cleaning up directory"

cd ..
rm -rf vm-config
